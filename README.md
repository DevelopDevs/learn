#Learn with DevelopDevs

Learn is the home for learning to code with DevelopDevs. Each language has it's own repository with the naming convention learn-LANGUAGE. Currently there is only one language that is properly supported, [Go/Golang](https://github.com/DevelopDevs/learn/blob/master/README.md#gogolang).

##Course Overviews
Overviews of all the major courses provided by DevelopDevs.

###Go/Golang

[Checkout the Course](http://github.com/DevelopDevs/learn-go)

The Go course is a great way to get started learning functional programming (Programming that allows you to complete tasks and perform functions). You will be introduced to basic programming concepts that are present in most if not all functional programming languages, these concepts include, printing, managing variables and constants, for loops, if else statements, and more.

###Python
*Limited resources*

[Checkout the Course](http://github.com/DevelopDevs/learn-python)

Python is a common first functional programming language, and is often taught in schools and universities to introduce students to programming concepts. As with most courses, Learn Python will step you through all the important programming concepts present in Python, with simple enough explanations for those learning their first language, and short lessons to help experienced coders speed through the lessons.
